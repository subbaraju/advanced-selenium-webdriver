package base;

public class TestUtilities extends BaseTest {

    protected void sleep(long mills){
        try {
            Thread.sleep(mills);
        } catch ( InterruptedException e) {
            e.printStackTrace();
        }
    }
}
