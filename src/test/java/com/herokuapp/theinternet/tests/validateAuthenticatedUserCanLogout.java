package com.herokuapp.theinternet.tests;

import base.TestUtilities;
import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.herokuapp.theinternet.pages.LoginPage;
import com.herokuapp.theinternet.pages.SecureAreaPage;
import com.herokuapp.theinternet.pages.WelcomePageObject;

public class validateAuthenticatedUserCanLogout extends TestUtilities {



	@Parameters({ "username", "password", "expectedMessage"})
	@Test
	public void validateLoginAndMessages(@Optional("tomsmith") String username,
										 @Optional("SuperSecretPassword!") String password,
									     @Optional("You logged into a secure area!") String expectedMessage)  {
		log.info("Starting logIn test");

		// open main page
		WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
		//welcomePage.openPage();

		// Click on Form Authentication link
		LoginPage loginPage = welcomePage.clickFormAuthenticationLink();

		// execute log in
		SecureAreaPage secureAreaPage = loginPage.logIn("tomsmith", "SuperSecretPassword!");
		sleep(2000);

		// log out button is visible
		Assert.assertTrue(secureAreaPage.isLogOutButtonVisible(), "LogOut Button is not visible.");

		//Log out user is clicked
		secureAreaPage.logOutUser();


	}



}
