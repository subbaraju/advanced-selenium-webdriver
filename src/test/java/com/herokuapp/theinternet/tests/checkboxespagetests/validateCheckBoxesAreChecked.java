package com.herokuapp.theinternet.tests.checkboxespagetests;

import base.TestUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.herokuapp.theinternet.pages.CheckBoxesPage;
import com.herokuapp.theinternet.pages.WelcomePageObject;

public class validateCheckBoxesAreChecked extends TestUtilities {

    @Test
    public void selectingTwoCheckBoxes(){
        log.info("Starting selectingTwoCheckBoxes");

        //Open main page
        WelcomePageObject welcomePage = new WelcomePageObject(driver, log);

        //Click on the CheckBoxes link
        CheckBoxesPage checkBoxesPage = welcomePage.clickCheckBoxesLink();

        //Click all Checkboxes
        checkBoxesPage.selectAllCheckboxes();

        //Assert all Checkboxes were clicked
        Assert.assertTrue(checkBoxesPage.areAllCheckboxesChecked(), "Not all boxes were checked");


    }
}

