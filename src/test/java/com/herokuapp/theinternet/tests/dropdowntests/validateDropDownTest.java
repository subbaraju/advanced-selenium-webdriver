package com.herokuapp.theinternet.tests.dropdowntests;

import base.TestUtilities;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.herokuapp.theinternet.pages.DropdownPage;
import com.herokuapp.theinternet.pages.WelcomePageObject;

public class validateDropDownTest extends TestUtilities {

    @Test
    public void selectOptionTwoDropDownTest() {
        log.info("Starting optionTwoTest");

        // open main page
        WelcomePageObject welcomePage = new WelcomePageObject(driver, log);
        welcomePage.openPage();

        // Click on Dropdown link
        DropdownPage dropdownPage = welcomePage.clickDropdownLink();

        // Select Option 2
        dropdownPage.selectOption(2);

        // Verify Option 2 is selected
        String selectedOption = dropdownPage.getSelectedOption();
        Assert.assertTrue(selectedOption.equals("Option 2"),
                "Option 2 is not selected. Instead selected - " + selectedOption);
    }
}
